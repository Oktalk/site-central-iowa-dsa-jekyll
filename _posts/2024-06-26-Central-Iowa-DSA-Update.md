---
layout: post
title: July, Central Iowa DSA Update
subtitle: In this newsletter -- TRANSIT WORKING GROUP MEETING, MASS TRANSIT WEBINAR WATCH PARTY, HOUSING WORKING GROUP MEETING, WORKERS ROUND TABLE, READING GROUP and more.
thumbnail-img: "https://actionnetwork.org/unlayer_files/1719188620425-Central+Iowa+DSA+Transit+Working+Group+%281%29.png"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---

Central Iowa DSA Update - July 2024
============================

Comrade,

Here is your July CIDSA Update. We have a lot of events coming up in July. On top of that, we're excited to announce that we have a [**website**](https://www.centraliowadsa.org/?link_id=0&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) now! The website features an [**event calendar**](https://www.centraliowadsa.org/events/?link_id=1&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) you can subscribe to to receive updates right to your phone calendar.

Upcoming Chapter Events
=======================

#### TRANSIT WORKING GROUP MEETING

![](https://actionnetwork.org/unlayer_files/1719188620425-Central+Iowa+DSA+Transit+Working+Group+%281%29.png)

Join the second meeting of our Transit Working Group **tonight at 6 PM**. We will be reporting our research findings on the various players in the DART funding "crisis" and discussing next steps. We will meet at the Des Moines Central Library (1000 Grand Ave, Des Moines), upstairs in the large study room.

If you want to receive special updates on the fight for public transit in central Iowa, sign up [**here**](https://actionnetwork.org/forms/central-iowa-dsa-transit-wg-interest-form/?link_id=2&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update).

[**Fill out the Transit WG Interest Form**](https://actionnetwork.org/forms/central-iowa-dsa-transit-wg-interest-form/?link_id=4&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)

#### MASS TRANSIT WEBINAR WATCH PARTY

[![](https://actionnetwork.org/unlayer_files/1719419010827-GNDCC_Transit_Mass_Call_AN_banner.png)](https://actionnetwork.org/forms/central-iowa-dsa-mass-transit-call-watch-party?clear_id=true&link_id=5&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)

Be part of Central Iowa DSA's delegation to DSA's national mass transit webinar! We will watch the webinar as a group, then discuss it afterwards. We will meet a little before 7 PM on Sunday, 6/30. **RSVP for location.**

[**RSVP Here!**](https://actionnetwork.org/forms/central-iowa-dsa-mass-transit-call-watch-party?clear_id=true&link_id=7&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)

#### HOUSING WORKING GROUP MEETING

![](https://actionnetwork.org/unlayer_files/1719419865490-Housing+Working+Group+65.png)

Join our Housing Working Group to discuss opportunities for fighting predatory landlords in central Iowa. We will meet at the Central Library (1000 Grand Ave, Des Moines) at 6 PM on Monday, 7/1.

#### WORKERS ROUND TABLE

![](https://actionnetwork.org/unlayer_files/1719420306566-Workers+Round+Table.png)

Join fellow socialists for a relaxed, informal conversation about work. Commiserate over problems and share ideas about how to organize to solve them. Bonus points if you bring a coworker! We will meet at Ingersoll Tap (2837 Ingersoll Ave, Des Moines) at 5:30 PM on Wednesday, 7/10.

#### READING GROUP: *CLIMATE CHANGE AS CLASS WAR*

![](https://actionnetwork.org/unlayer_files/1719424097654-Central+iowa+DSA+%282%29.png)

In *Climate Change as Class War*, Matt Huber analyzes the state of the climate movement, examining the Green New Deal to make the case that the climate movement should shift its targeting from a narrow band of middle-income professionals to broader swaths of working class through industrial policy and strategic engagement with the labor movement.

What's even better, this book ties in with our transit work! **If you would like a digital copy of the book, free of charge, reply to this email and let us know.** Beaverdale Books is also offering 10% off hard copies.

We will be meeting at Beaverdale Books at 6 PM on Sunday, 7/21. 

#### TELL YOUR LEGISLATORS: STOP REPRESSING THE BDS MOVEMENT

[![](https://actionnetwork.org/unlayer_files/1719425717401-Untitled+design.png)](https://dsausa.us/NoRepression?link_id=8&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) 

The legislative session ended, and the only bills about the genocide in Gaza to pass out of the Iowa statehouse were pro-genocide. Use the link below to tell your state legislator that that is unacceptable. Demand they repeal Iowa's anti-BDS laws.

[**Send Your Email Now!**](https://dsausa.us/NoRepression?link_id=10&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)

Upcoming State and National Events
==================================

#### SUMMER ORGANIZING WORKSHOPS

[![](https://actionnetwork.org/unlayer_files/1719434121949-Strategic_Campaign_Planning.png)](https://actionnetwork.org/event_campaigns/dsa-summer-organizing-workshops?link_id=11&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)

National is offering a variety of online organizing workshops throughout the summer. Join DSA members from around the country as you increase your organizing capacity!

Our chapter will be focusing on the "Strategic Campaign Planning" session on Wednesday, 7/24, at 7 PM.

[**RSVP Here**](https://actionnetwork.org/event_campaigns/dsa-summer-organizing-workshops?link_id=13&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)

Upcoming Community Events
=========================

-   6/27: [Safe Drinking Water Act webinar](https://www.facebook.com/events/2999744136867712?link_id=14&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update). Iowa Citizens for Community Improvement. 6:30 PM, online.

-   6/29: Hamza Miltaha: "[Turning Points in Palestinian History](https://www.instagram.com/p/C8qnMumO8tm/?link_id=15&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)." Des Moines for Palestine. 11:30 AM, West Des Moines RecPlex Center.

-   6/29: [People's Gathering for Palestine](https://www.instagram.com/p/C8ht_DFx6vS/?img_index=1&link_id=16&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update). Party for Socialism and Liberation--Iowa and Artivists of Iowa. 3-7 PM, Water Works Amphitheater.

-   6/29: [DART Crawl](https://www.instagram.com/p/C7txzZeuUjB/?img_index=1&link_id=17&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update). Momentum DSM. 1:00 PM, DART Central Station.

-   7/3: [Immigration "Know Your Rights" Train the Trainer Session](https://us02web.zoom.us/meeting/register/tZIsduGgqDIqHdFxjQGh5QCYUpXYir55160n#/registration?link_id=18&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update). Iowa Movement for Migrant Justice. 12:00 PM, online.

-   7/13: Iowa CCI Open House. 10:15 AM, Iowa CCI office.

DSA Spotlight
=============

The DSA Spotlight is a new section of the CIDSA *Update* dedicated to shining a spotlight on one national committee, working group, campaign, or resource each month. This month, we're looking at the [***Democratic Left***](https://www.dsausa.org/democratic-left/?link_id=19&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update).

[![](https://actionnetwork.org/unlayer_files/1719406944359-Screenshot+2024-06-26+at+8.01.56%E2%80%AFAM.png)](https://www.dsausa.org/democratic-left/?link_id=20&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)

The *Democratic Left* is DSA's magazine. Members submit the bulk of the content. Recently, members have published [debates](https://www.dsausa.org/democratic-left/dsa-should-re-endorse-aoc-as-a-rallying-point-for-democratic-socialism/?link_id=21&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) on [Congressional endorsements](https://www.dsausa.org/democratic-left/i-miss-the-old-aoc/?link_id=22&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) and pieces on [car dependency](https://www.dsausa.org/democratic-left/against-king-car/?link_id=23&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update); the [relationship between local Socialists in Office and their chapters](https://www.dsausa.org/democratic-left/chicago-dsa-is-defending-the-right-to-protest-genocide-at-the-dnc/?link_id=24&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) against a backdrop of genocide; [organizing for the Uncommitted campaign](https://www.dsausa.org/democratic-left/putting-palestine-on-the-ballot-an-interview-with-uncommitted-new-jersey-organizers/?link_id=25&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update); and more.

Any DSA member may pitch an article to the volunteer editors by emailing <dl@dsacommittees.org> with their pitch for an article of 800-1800 words in length. Pitches should include an opening sentence, an outline of the argument, and a closing statement or call to involvement for other chapters. The *Democratic Left* is also seeking volunteer writers, editors, copy editors, and graphic designers. If you're interested, [apply here](https://democraticleft.dsausa.org/apply/?link_id=27&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update).

DSA in the News
===============

-   AIPAC [spent $14.5 million](https://www.politico.com/news/2024/06/25/jamaal-bowman-loses-george-latimer-00164997?link_id=28&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) to replace DSA-endorsed Congressman Jamaal Bowman with [segregationist](https://inthesetimes.com/article/george-latimer-jamaal-bowman-new-york-primary?link_id=29&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) George Latimer. It was the most expensive Democratic primary in US history.

-   Madison DSA recently joined a coalition to win [free school lunches](https://spectrumnews1.com/wi/milwaukee/news/2024/05/28/madison-organizations-push-for-free-school-meals?link_id=30&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update) for all Madison students.

-   Atlanta DSA is joining other left organizations in the city to [protest the first presidential debate](https://www.record-bee.com/2024/06/25/we-are-protesting-both-of-them-presidential-debate-draws-activists-ire/amp/?link_id=31&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update).

-   Seattle DSA recently weighed, and [ultimately rejected](https://www.thestranger.com/news/2024/06/10/79552806/the-pro-palestine-challengers#:~:text=Where%E2%80%99s%20the%20Movement%3F?link_id=32&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update), Congressional endorsements.

-   Portland DSA, which has an exceptionally strong Labor Working Group, [stood behind the Portland Association of Teachers](https://www.wweek.com/news/schools/2024/06/23/school-board-members-weigh-in-on-teacher-unions-palestine-advocacy/#:~:text=One%20noteworthy%20exception?link_id=33&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update)'s curriculum on Palestine and the genocide in Gaza.

Finally, while any and every worker is welcome at our events, they're only possible because members organize them and, most importantly, fund them. DSA is entirely dues-funded, meaning our organization isn't beholden to anyone but us. If you haven't done so yet, [**join DSA today**](https://dsausa.org/join?link_id=34&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-7&email_referrer=&email_subject=central-iowa-dsa-update), and remember that our chapter receives 30% of all **monthly** dues, so be sure to select that option to support our local organizing!

In solidarity,

Central Iowa DSA
