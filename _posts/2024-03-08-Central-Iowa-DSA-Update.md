---
layout: post
title: March 8th, Central Iowa DSA Update
subtitle: In this newsletter -- St. Patrick's Day Social, March for Gaza, Prison Abolition Working Group and a chance to tell the IDP Stop Supporting Genocide!
thumbnail-img: "https://actionnetwork.org/unlayer_files/1709868172116-St.+Paddy's+Banner.png"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---


Central Iowa DSA Update
=======================

Hello, comrades! Here is your Central Iowa DSA update. We have several exciting events to share with you this month! To make all of this happen, we need **you**, though. If you haven't done so already, please [renew your DSA dues](https://act.dsausa.org/donate/renew/?link_id=1&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update). The best is yet to come!

Upcoming Chapter Events
=======================

St. Patrick's Day Social
------------------------

[![](https://actionnetwork.org/unlayer_files/1709868172116-St.+Paddy's+Banner.png)](https://actionnetwork.org/events/central-iowa-dsa-st-paddys-social/?link_id=2&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update)

Join us this Thursday, March 14th, at Ingersoll Tap for our St. Patrick's Day Social! We'll be talking Palestine solidarity, public transit, reading groups, and building the socialist movement in Des Moines. Be sure to bring a friend, take the bus to get there safely and responsibly, and sign in for your drink ticket! RSVP below so we can get a rough head count.

[**RSVP Now!**](https://actionnetwork.org/events/central-iowa-dsa-st-paddys-social/?link_id=4&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update)

March for Gaza
--------------

![](https://actionnetwork.org/unlayer_files/1709880660138-Add+a+heading+(4).png)

Join us next Saturday, March 16th, at 12:00 PM for a march for Palestinian freedom. The starting location will be announced closer to the march; be sure to watch the chapter's social media for updates.

Prison Abolition Working Group
------------------------------

![](https://actionnetwork.org/unlayer_files/1708390062967-PAWG+White.jpeg)

Join our Prison Abolition Working Group on Wednesday, March 27th, at 5:45 PM at the Forest Avenue Library (1326 Forest Ave, Des Moines). We will be discussing ways to increase the working group's capacity, the prison pen pal program, and more.

Tell the IDP: Stop Supporting Genocide!
---------------------------------------

[![](https://actionnetwork.org/unlayer_files/1707105027479-Stop+the+Repression+AN+Banner+(1).png)](https://actionnetwork.org/letters/stop-the-repression-ceasefire-now/?link_id=5&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update)

It's been 5 months since Israel began its genocide against the people of Gaza. It's been 4 months since the Iowa Democratic Party [threatened student leaders](https://x.com/iowademocrats/status/1719900886582067302?s=20&link_id=6&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) at the University of Iowa for expressing their opposition to that genocide. It's been 3 months since the party [tripled down](https://www.desmoinesregister.com/story/news/politics/2023/12/14/iowa-state-democrats-iowa-democratic-party-mend-fences-after-disagreement/71910174007/#:~:text=?link_id=7&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) on its support for the genocide in Gaza, and since Iowa House Democrats [voted for HR 101](https://x.com/DSM_DSA/status/1748043447750037587?s=20&link_id=8&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update), the pro-genocide resolution. It's been one week since ceasefire activists [picketed an IDP fundraiser](https://x.com/Ollie_XVX/status/1764405908849713526?s=20&link_id=9&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) in Iowa City. The Iowa Democratic Party just doesn't get that working class Iowans stand with Gaza. We're going to have to turn the pressure up to make sure they understand us. In the mean time, take a minute to [send them a pre-filled email](https://actionnetwork.org/letters/stop-the-repression-ceasefire-now/?link_id=10&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) demanding that they stop supporting Israel's genocide in Gaza.

[**Send an Email Now!**](https://actionnetwork.org/letters/stop-the-repression-ceasefire-now/?link_id=12&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update)

Upcoming National Events
========================

-   Ongoing through 3/12: Join Washington state DSA members at one of several "Uncommitted" [phone banks](https://www.dsausa.org/no-money-for-massacres-phonebanks/?link_id=0&can_id=6cd4a944ab0853240c109f7f3cff1830&source=email-urgent-join-a-phonebank-to-tell-michigan-voters-vote-uncommitted-for-a-ceasefire&email_referrer=email_2215571&email_subject=phonebank-to-urge-michigan-voters-to-vote-uncommitted-to-support-a-ceasefire&link_id=13&can_id=078080b221b070660325c17d0bc3ae19&email_referrer=&email_subject=central-iowa-dsa-update).
-   Sunday, 3/23, 9 AM: DSA International Committee "[Tax Justice for Reparations](https://actionnetwork.org/events/session-2-tax-justice-for-reparations?link_id=14&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update)" seminar.
-   Ongoing: Tell Grassley, Ernst, and Nunn to [let Gaza live](https://actionnetwork.org/letters/ceasefire-now-resolution-no-money-for-massacres?link_id=15&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) and support an immediate ceasefire.

Upcoming Community Events
=========================

-   3/30: Join Momentum DSM for another [DART Crawl](https://www.facebook.com/events/314394587757898?link_id=16&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update). This crawl's theme is "Tacos and Margaritas." Participants will get to see the south side through DART's eyes, and, of course, enjoy tacos and margs.
-   Ongoing: [Email your state senator](https://actionnetwork.org/letters/contact-your-senator-to-protect-our-water/?link_id=17&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) and demand that they kill SF 2371, which would allow agribusiness interests to dump manure wherever they want, poisoning our water, driving a water crisis in Des Moines, and giving Iowans the second-highest cancer rate in the nation.
-   Ongoing: [Email](https://actionnetwork.org/letters/save-dart-tell-dsm-city-council-to-approve-the-franchise-fee?source=direct_link&link_id=18&can_id=078080b221b070660325c17d0bc3ae19&email_referrer=&email_subject=central-iowa-dsa-update) the Des Moines city council and tell them not to kill public transit in Des Moines.

DSA in the News
===============

-   Metro DC DSA and New York City DSA teamed up and [forced Biden's motorcade to reroute](https://x.com/DemSocialists/status/1765922563446767909?s=20&link_id=19&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) on its way to the State of the Union address.
-   In Michigan, Minnesota, and beyond, "Uncommitted" is [winning big](https://jacobin.com/2024/03/uncommitted-vote-joe-biden-reelection?link_id=20&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) in Democratic primaries around the country, often led by DSA chapters!
-   Seattle DSA's [Raise the Wage Renton](https://seattledsa.org/2024/02/renton-raises-the-wage-workers-win-20-29-hour-starting-in-july-2024/?link_id=21&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) campaign [won](https://www.king5.com/article/news/politics/elections/renton-initiative-minimum-wage-february-2024-special-election/281-57ee7da6-0003-437b-b840-c0c611d97566?link_id=22&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) in a special election. This win makes the town's minimum wage the highest in the country.
-   [Mid-Missouri DSA](https://x.com/MidMoDSA/status/1759805805576982806?s=20&link_id=23&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) and [Mizzou YDSA](https://x.com/MizzouYDSA/status/1760422975281537365?s=20&link_id=24&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) got a [Safe Haven ordinance](https://www.columbiamissourian.com/news/local/everyone-wants-to-feel-safe-city-council-approves-safe-haven-for-lgbtq-residents/article_c8ae1654-cf54-11ee-8f25-c7f066d2a3f3.html?link_id=25&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) through the Columbia city council. The ordinance establishes enforcement of Missouri's transphobic laws as the lowest enforcement priority for local law enforcement and establishes other protections. The Des Moines city council has repeatedly refused to pass similar ordinances.
-   St. Louis DSA [recently launched](https://www.stltoday.com/news/local/education/new-push-for-tax-exempt-washington-u-to-donate-to-st-louis-university-city-schools/article_434fd4d2-cc27-11ee-bdcb-0b7b6f5ab380.html?link_id=26&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) its "Wash U Owes U" campaign for a "payment in lieu of taxes" agreement with Washington University---an elite private university with a $12.5 billion endowment that currently does not pay taxes to support St. Louis's underfunded public services. Locally, 40% of the property in Des Moines is tax-exempt.

Renew Your DSA Dues!
====================

Only socialist cash beats capitalist trash. While we may never out-fundraise the ruling class, a little money goes a long way. When you [renew your DSA dues](https://act.dsausa.org/donate/renew/?link_id=27&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update) under the "monthly" or "solidarity dues" options, our chapter gets 20% of that revenue to use right here in Des Moines! So far, we've used those funds to print signs, make t-shirts, purchase clipboards for canvassing, and more. We hope you'll join us this spring.

[**Renew Your Dues Today!**](https://act.dsausa.org/donate/renew/?link_id=29&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-3&email_referrer=&email_subject=central-iowa-dsa-update)

In solidarity,

Central Iowa DSA
