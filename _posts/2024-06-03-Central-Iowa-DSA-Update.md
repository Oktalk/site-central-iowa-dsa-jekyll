---
layout: post
title: June 3th, Central Iowa DSA Update
subtitle: In this newsletter -- HOUSING WORKING GROUP MEETING, MARCH WITH US AT THE PRIDE PARADE, READING GROUP WOMEN RACE AND CLASS, TRANSIT WORKING GROUP MEETING, DONATE TO THE UAW 4811 STRIKE FUND and more.
thumbnail-img: "https://actionnetwork.org/unlayer_files/1717000738532-D8A74799-E82F-4075-8011-10326EDE48F9.png"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---

Central Iowa DSA Update - June 2024
===================================

Hello, comrade! Welcome to your June Central Iowa DSA Update. We have ***a lot***going on this month, so be sure to come out to one of our events!

Upcoming Chapter Events
=======================

#### HOUSING WORKING GROUP MEETING

[![](https://actionnetwork.org/unlayer_files/1717000738532-D8A74799-E82F-4075-8011-10326EDE48F9.png)](https://actionnetwork.org/events/housing-working-group-meeting-11?clear_id=true&link_id=0&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

We're proud to announce the re-launch of our Housing Working Group! If you're interested in organizing for housing justice, this is the group for you. We'll have an open discussion about housing policy and capitalism, as well as a potential campaign to challenge corporate landlords right here in the Des Moines area. We will gather at the Des Moines Central Library at 6 PM on Wednesday, June 5th. Use the button below to RSVP.

[RSVP Here](https://actionnetwork.org/events/housing-working-group-meeting-11?clear_id=true&link_id=2&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

#### MARCH WITH US AT THE PRIDE PARADE!

[![](https://actionnetwork.org/unlayer_files/1717335847388-Pride+Parade+IG.png)](https://actionnetwork.org/events/central-iowa-dsa-pride-parade-rsvp?link_id=3&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

Join Central Iowa DSA at the 2024 Pride parade! Lineup begins at 10:30 AM on Sunday, June 9th. The parade will begin at 12 PM. **Use the button below to RSVP.** This is important so we can share our location in the lineup on the morning of the parade.

[RSVP Here](https://actionnetwork.org/events/central-iowa-dsa-pride-parade-rsvp?link_id=5&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

#### READING GROUP: *WOMEN, RACE, AND CLASS*

![](https://actionnetwork.org/unlayer_files/1717456720732-the+impact+participation+in+the+abolitionist+movement+had+on+women%E2%80%99s+political+consciousness.png)

Join us for a discussion of Angela Davis's seminal *Women, Race, and Class*. This classic text examines the challenges and potential of organizing emancipatory movements across differences and offers an important analysis of American political economy.

We will meet on Sunday, June 9th, at 6 PM at Peace Tree Brewing in Des Moines. If you would like a digital copy of the book, free of charge, simply reply to this email or DM any of the chapter socials.

#### TRANSIT WORKING GROUP MEETING

![](https://actionnetwork.org/unlayer_files/1717250981975-Central+Iowa+DSA+Transit+Working+Group.png)

Join us for the launch of our Transit Working Group! This first meeting will be informal. We'll meet fellow bus riders and enthusiasts, talk about what the bus means to us, and begin to discuss ways to save it. We will meet on Wednesday, June 12th, at 6 PM at the Des Moines Central Library.

Upcoming State and National Events
==================================

#### DONATE TO THE UAW 4811 STRIKE FUND

[![](https://actionnetwork.org/unlayer_files/1717255730644-UCLA+4811+Strike+Fund.png)](https://bayresistance.org/hardship-solidarity-fund-for-uaw-4811-members/?link_id=6&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

Donate to the UAW 8411 strike fund! UAW 4811 represents academic workers at the University of California--one of the West Coast's largest employers. After university administrators called police and Zionist vigilantes onto campuses to assault students, faculty, staff, and community members protesting the genocide in Gaza, the workers are using their collective power to shut the university down. The DSA National Labor Commission has asked members to donate to their strike fund, which you can do using the button below. **If you fill out [this form](https://docs.google.com/forms/d/e/1FAIpQLScl7Oo0EZ5AP5A5wF_EmW0UJHMf1qdrJsKx_Ux8i9bzSRMM-g/viewform?link_id=7&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update), DSA will match your donation!**

[Donate Here](https://bayresistance.org/hardship-solidarity-fund-for-uaw-4811-members/?link_id=9&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

[Matching Form Here](https://docs.google.com/forms/d/e/1FAIpQLScl7Oo0EZ5AP5A5wF_EmW0UJHMf1qdrJsKx_Ux8i9bzSRMM-g/viewform?link_id=11&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

#### HELP LAS VEGAS DSA: PHONE BANK FOR VAL THOMASON

[![](https://actionnetwork.org/unlayer_files/1717338092670-Val+Thomason.jpeg)](https://lvdsa.org/events/?link_id=12&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

Help elect former Las Vegas DSA chair and Teamster organizer Val Thomason to the Nevada state legislature! There are virtual phone banks on Tuesday and Wednesday at 8 PM Central.

[Find a Phone Bank Here](https://lvdsa.org/events/?link_id=14&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update)

Upcoming Community Events
=========================

-   The Des Moines City Council is attempting to privatize our public transit system by selling it off to a venture capital funded ride hailing company. This will drastically decrease service. PSL Iowa [will be at the next city council meeting](https://www.instagram.com/p/C7mNlz6OcYG/?img_index=1&link_id=15&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update), demanding the council cancel the first contract with Via, expand transit service (instead of cutting it), and call for a ceasefire in Gaza. They will be at City Hall from 5-7 PM on Monday, June 3rd.
-   Shop local queer artists and crafters and enjoy live music at [People's Pride](https://www.instagram.com/dsmpeoplespride/?link_id=16&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update). This sober, police- and corporation-free event will be at Waterworks Park from 12-5 PM on Saturday, June 15th.
-   Momentum DSM is hosting another [DART Crawl](https://www.facebook.com/permalink.php?story_fbid=pfbid025wApWoakbKWevZMyTYtXzMNdLdzJcaHEVP2t3HXNTfSUYRdJkhxqyPUB8HiUiXnzl&id=100088564495677&link_id=17&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update) on Saturday, June 29th, at 1 PM. This is an excellent opportunity to try the bus out in a fun, social environment.

DSA in the News
===============

-   DSA candidate J.P. Lyninger [defeated an establishment incumbent](https://www.lpm.org/news/2024-05-21/louisville-metro-council-races-are-set-following-tuesday-primary?link_id=18&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update) for a seat on Louisville, KY's Metro Council. He ran on a [platform](https://jpforlouisville.com/issues/?link_id=19&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update) of improving and expanding public transit, bringing the local power utility into public ownership, investing in public housing and a municipally-owned grocery store, defunding the police, and more.
-   DSA candidate Gabriel Sanchez [won his primary](https://atlantaciviccircle.org/2024/05/22/gen-z-atlanta-takeaways-georgia-general-primary-election/?link_id=20&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update) against a powerful incumbent in the Georgia House. The challenges Atlanta DSA faced were very similar to challenges our chapter is likely to face if we decide to run a candidate.
-   Tucson DSA [organized a rally](https://www.kgun9.com/news/community-inspired-journalism/midtown-news/democratic-socialists-of-america-push-to-replace-tep-with-publicly-owned-company?link_id=21&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update) to demand replacing the city's private, for-profit energy utility with a public utility.
-   DSA members in California are on the front lines of the [historic Palestine solidarity strike](https://inthesetimes.com/article/uaw-4811-strike-ucla-university-of-california-israel-palestine-students-rafah-gaza?link_id=22&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update) (launched as an unfair labor practice strike) across the University of California system.
-   DSA has joined the Council on American-Islamic Relations in [suing Texas Governor Greg Abbot](https://www.houstonpublicmedia.org/articles/civil-rights/protests/2024/05/20/487942/student-groups-sue-texas-governor-universities-over-executive-order-on-campus-free-speech/?link_id=23&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-5&email_referrer=&email_subject=central-iowa-dsa-update) for his unconstitutional suppression of pro-Palestinian and anti-genocide speech.

We'll see you out there! If you have any questions, ideas, or anything else to share, just reply to this email.

In solidarity,

Central Iowa DSA
