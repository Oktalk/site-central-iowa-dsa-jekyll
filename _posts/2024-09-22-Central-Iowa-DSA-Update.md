---
layout: post
title: September 22th, Central Iowa DSA Update
subtitle: In this newsletter -- Reading Group, Housing Working Group Meeting, Transit Working Group Meeting, The Settlers Screening, General Meeting
thumbnail-img: "https://actionnetwork.org/unlayer_files/1725812974391-Central+Iowa+DSA+%284%29.png"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---

![](https://actionnetwork.org/unlayer_files/1709856217672-Round+Logo+%281%29.png)

September Central Iowa DSA Update
=================================

August was a big month for DSA. Thousands marched at the DNC against the genocide in Gaza, and chapters supported tens of thousands of striking workers in key industries and won historic victories for the Palestine movement. September will be a big month for our chapter. We have several public events planned; working groups will continue planning campaigns for housing justice and public transit; and, most importantly, we will have our first general membership meeting! Read on for details.

Upcoming Chapter Events
=======================

#### Reading Group: *Capitalist Realism*

![](https://actionnetwork.org/unlayer_files/1725673543770-82424+Reading+Group+%281%29.png)

Join us for a discussion of Mark Fisher's *Capitalist Realism*. We'll meet on Sunday, September 8th at 6:00 PM at Beaverdale Books (2629 Beaver Avenue, Des Moines).

#### Housing Working Group Meeting

![](https://actionnetwork.org/unlayer_files/1725673725487-Housing+Working+Group+65+%284%29.png)

The Housing Working Group will meet on Monday, September 9th at 6:00 PM at the Central Library (1000 Grand Ave, Des Moines). There will be a presentation on tenant organizing, and we will discuss an update on the mobile home park canvass plan.

#### Transit Working Group Meeting

![](https://actionnetwork.org/unlayer_files/1725665168014-Central+Iowa+DSA+Transit+Working+Group+%282%29.png)

The Transit Working Group will meet on Wednesday, September 11th at 6:00 PM at the Central Library (1000 Grand Ave, Des Moines). We'll be discussing our recent bird-dogging of Connie Boesen, event tabling, the formal campaign proposal, and beginning to target suburban city council members. Please join us! And if you have any questions, just reply to this email.

#### *The Settlers* Screening

[![](https://actionnetwork.org/unlayer_files/1725673949448-The+Settlers+Screening+Insta.png)](https://actionnetwork.org/events/film-screening-the-settlers?link_id=0&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

Our chapter is hosting a screening of *The Settlers*, an award-winning documentary exploring the lives and motivations of Israeli settlers in the West Bank. The film presents an unparalleled on-the-ground picture of settler colonialism unfolding in real time. A short panel discussion may follow. The screening will be on Thursday, September 26th. The location is still TBD; we will send a followup email when the location is determined.

[RSVP Here](https://actionnetwork.org/events/film-screening-the-settlers?link_id=2&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

#### General Meeting

[![](https://actionnetwork.org/unlayer_files/1725812974391-Central+Iowa+DSA+%284%29.png)](https://actionnetwork.org/events/central-iowa-dsa-general-meeting-september-2024?link_id=3&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

It's time! On Sunday, September 29th, join fellow DSA members for the first official general meeting since 2020! General meetings are opportunities for DSA members to come together as a community and discuss the formation of our chapter and plan for future projects. They feature discussions of chapter operations and future projects; resolutions and endorsements; political discussions of local, state, national, and international affairs; and more! General meetings are intended to set the policy that the steering committee will carry out.

For our first general meeting, there will be a short introduction to DSA, a summary of current chapter activities, an opportunity to propose new working groups (groups of members who work on specific issues or serve specific functions), and formal officer nominations.

If you would like to nominate yourself or someone else for an officer position, use the form below. We will also take nominations from the floor.

Only members in good standing will be able to make motions and vote during the meeting. If you need to renew your dues, do so [here](http://dsausa.org/renew?link_id=4&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update), and save a screenshot to receive your voting card for the meeting.

We will meet at 3:00 PM at the Johnston Public Library. Reply to this email if you have any questions.

[RSVP Here](https://actionnetwork.org/events/central-iowa-dsa-general-meeting-september-2024?link_id=6&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

[Nominate a chapter officer here!](https://docs.google.com/forms/d/e/1FAIpQLSeMzv9Zj9h4Ykr2mQ2Ve6A0eyE0spKd6uyafZjaKcuHePrWiQ/viewform?usp=sf_link&link_id=8&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

Upcoming State and National Events
==================================

#### People's Tribunal on US State-Backed Violence in the Philippines

[![](https://actionnetwork.org/unlayer_files/1725676092803-IC+Phillipines.jpeg)](https://dsausa.zoom.us/meeting/register/tZIkcOqhqzkrEtEL2gpoiG8Hzabq695uA4-H#/registration?link_id=9&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

The DSA International Committee is hosting a report on the recent [International People's Tribunal on US State-Backed Violence in the Philippines](https://ichrp.net/international-peoples-tribunal-us-marcos-duterte-guilty-of-massive-war-crimes/?link_id=10&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update). Monday, 9/9, 7:30 PM.

[Register Here](https://dsausa.zoom.us/meeting/register/tZIkcOqhqzkrEtEL2gpoiG8Hzabq695uA4-H#/registration?link_id=12&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

#### Solidarity Across Borders

[![](https://actionnetwork.org/unlayer_files/1725676701213-Solidarity+Across+Borders.jpeg)](https://actionnetwork.org/campaigns/solidarity-across-borders-webinar-series/?link_id=13&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

The DSA International Committee is also hosting two webinars on international solidarity in the Americas. The 9/12 webinar is on resisting the international reach of ICE and the CBP, and the 9/26 webinar is on how border imperialism empowers exploitative employers and how labor is fighting back.

[Register Here](https://actionnetwork.org/campaigns/solidarity-across-borders-webinar-series/?link_id=15&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

#### EWOC Training Series

[![](https://actionnetwork.org/unlayer_files/1725677188210-Screenshot+2024-09-06+at+9.45.45%E2%80%AFPM.png)](https://workerorganizing.org/training/?link_id=16&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

Learn the basics of workplace organizing in this four-part training series. You'll develop vital skills in identifying leaders, demands, and targets; developing escalation steps; and getting over your fear to fight the boss! The trainings are every Wednesday at 7:00 PM. Hit the button below and scroll to the bottom to register.

[Register Here](https://workerorganizing.org/training/?link_id=18&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

Upcoming Community Events
=========================

-   Labor Notes [*Righting the Wrongs of Management Rights*](https://labornotes.org/events/2024/stewards-workshop-fighting-wrongs-management-rights?link_id=19&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update) training. 9/10/24, 6:30 PM. Online. $10 registration fee; no one turned away for lack of funds.
-   CCI Public School Strong [orientation](https://iowacci.ourpowerbase.net/civicrm/event/info?reset=1&id=3277&custom_511=enews&link_id=20&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update). 9/24/24, 6:00 PM. Online.
-   Momentum DSM [Urbanism Walk](https://www.instagram.com/p/C_JEw4eO3MH/?igsh=MWF6OWo2cWZneHJmeQ==&link_id=21&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update): 9/26/24, 4 PM, MLK & Ingersoll. Just don't miss the film screening!
-   Ongoing: Jefferson County Farmers and Neighbors [petition](https://www.jfaniowa.org/how-to-stop-the-eats-act?link_id=22&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update) against the agribusiness-sponsored EATS Act, which would ban the type of regulations needed to clean up our water.

DSA Spotlight
=============

The DSA Spotlight is a new section of the CIDSA *Update* dedicated to shining a spotlight on one national committee, working group, campaign, or resource each month. This month, we're looking at the [National Electoral Commission](https://electoral.dsausa.org/?link_id=23&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update).

[![](https://actionnetwork.org/unlayer_files/1725678742406-NEC.jpeg)](https://electoral.dsausa.org/?link_id=24&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update)

The National Electoral Commission is responsible for DSA's electoral work. The NEC trains DSA members on how to plan, run, and win electoral campaigns and helps chapters build effective electoral programs. It also [brings together](https://inthesetimes.com/article/democratic-socialist-dsa-conference-bernie-sanders-cori-bush?link_id=25&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update) the hundreds of DSA-endorsed elected officials across the country for training and strategy sessions.

#### How do DSA endorsements work, anyway?

The endorsement process begins when a candidate approaches a local DSA chapter and asks for the chapter's endorsement. Sometimes, chapters recruit members to run for office.

The local chapter decides whether or not to endorse the candidate, according to its own endorsement process. If a chapter needs more support with a campaign, or feels its endorsed candidate  is especially strong or exemplary of our organization, it can apply to the National Electoral Commission for a national endorsement.

The NEC deliberates over each endorsement application and recommends the strongest applications to the National Political Committee for approval. Resources are limited, so not every application will be approved. The NPC votes on whether or not to endorse each candidate recommended by the NEC. Endorsement expands the reach of campaigns across the entire national organization and makes candidates eligible for support from the DSA PAC.

Whether or not the national organization endorses a candidate, local chapters like ours are free to endorse any candidate they wish. If you're interested, you can watch a webinar on chapter endorsements and electoral work [here](https://youtu.be/-VHQl7gGgLo?feature=shared&link_id=26&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update). You can read a recent analysis of DSA's electoral organizing [here](https://rosalux.nyc/u-s-socialists-long-march-through-city-and-state-governments/?link_id=27&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update).

DSA in the News
===============


-   Chapter member Blake Iverson called on Iowa Democratic Party leaders to stop supporting the genocide in Gaza in a [post on *Bleeding Heartland*](https://www.bleedingheartland.com/2024/09/01/the-fourth-crusade-how-gaza-could-cost-democrats-the-election/?link_id=28&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update), a popular Iowa politics blog.
-   DSA co-chair Ashik Siddique [spoke](https://www.youtube.com/live/MpdA0Av7A9A?si=RqwymZh8aN-2eO_H&t=4876&link_id=29&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update) at the March on the DNC. His speech was broadcast across the nation and streamed across the world!
-   The Left on Red podcast is [beginning a series](https://open.spotify.com/playlist/5IjTae3QO8r05JwzuFfKn1?link_id=30&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update) on the delegation DSA sent to Cuba last spring.
-   Maine DSA and partners in the Maine Coalition for Palestine just won the [fourth divestment resolution in the country](https://www.commondreams.org/news/portland-maine-divests-israel-2669135363?link_id=31&can_id=078080b221b070660325c17d0bc3ae19&source=email-september-update-22&email_referrer=&email_subject=september-update) and the first on the east coast.

In solidarity,

Central Iowa DSA