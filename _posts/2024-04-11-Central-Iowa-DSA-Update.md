---
layout: post
title: April 11th, Central Iowa DSA Update
subtitle: In this newsletter -- READING GROUP, LABOR ROUND TABLE, MAY DAY PICNIC, ELECTION DISCUSSION CIRCLES, and more.
thumbnail-img: "assets/img/post/MayDayFlyer.jpg"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---

Central Iowa DSA Update
=======================

Here is your Central Iowa DSA update. We have many events to share this month!

Upcoming Chapter Events
-----------------------

#### READING GROUP

![](https://actionnetwork.org/unlayer_files/1712798293901-Reading+Group+Graphic.png) |

The Central Iowa DSA reading group will hold its first event at Smokey Row on Wednesday, April 17th, at 6:00 PM. We will be discussing "Bourgeois and Proletarians," the first chapter of the *Communist Manifesto*. This is a fun read that clearly lays out some of the key concepts of socialism. The text can be found [**here**](https://www.marxists.org/archive/marx/works/1848/communist-manifesto/ch01.htm?link_id=0&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore), and anyone who attends will receive a ballot to select our next reading!

#### LABOR ROUND TABLE

[![](https://actionnetwork.org/unlayer_files/1712798969711-CIDSA+Workers+Round+Table+(1).png)](https://actionnetwork.org/events/central-iowa-dsa-labor-round-table?link_id=1&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

Join fellow socialists for an informal discussion of our work lives and the possibilities for workplace organizing here in Central Iowa. We will be meeting at the Des Moines Central Library at 12:00 PM on Saturday, April 27th. Union members are especially encouraged to attend, but all are welcome. 

[**RSVP here!**](https://actionnetwork.org/events/central-iowa-dsa-labor-round-table?link_id=3&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

#### MAY DAY PICNIC

![May Day Event Flyer](../assets/img/post/MayDayFlyer.jpg)

Celebrate May Day with us! May Day is the original workers' day, first celebrated in 1890 to commemorate the socialist and anarchist martyrs killed by the capitalist state in Chicago, it is now celebrated by hundreds of millions of workers all over the world every year.

This year, DSA is joining with other local left organizations to put on a fun, family-friendly event. There will be a potluck, games, a historical display, speakers, and more. We will gather on Sunday, April 28th, at Evelyn K. Davis Park in Des Moines. The exact time is still to be determined, so watch our social media for updates.

#### TELL YOUR STATE LEGISLATOR AND THE IDP: STOP SUPPORTING GENOCIDE!

[![](https://actionnetwork.org/unlayer_files/1707105027479-Stop+the+Repression+AN+Banner+(1).png)](https://actionnetwork.org/letters/stop-the-repression-ceasefire-now/?link_id=4&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

After six months, the Iowa Legislature, along with the leadership of the Iowa Democratic Party, think they can keep supporting Israel's genocide without consequence. Use the link below to send them an email demanding they stop.

[**Send an email now!**](https://actionnetwork.org/letters/stop-the-repression-ceasefire-now/?link_id=6&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

Upcoming State and National Events
----------------------------------

#### ELECTION DISCUSSION CIRCLES

[![](https://actionnetwork.org/unlayer_files/1712800208934-2024_Election_Discussion_Circles.png)](https://actionnetwork.org/event_campaigns/discussion-circles?link_id=18&can_id=6cd4a944ab0853240c109f7f3cff1830&source=email-monthly-newsletter-march-2024-3&email_referrer=email_2272210&email_subject=monthly-newsletter-april-2024-_&link_id=7&can_id=078080b221b070660325c17d0bc3ae19&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

How should DSA approach the 2024 election? Join a small group of DSA members from around the country to discuss just that. Each discussion will be moderated by a member of the National Political Committee, giving you a direct line to national leadership. There are several scheduling options to choose from.

[**RSVP here!**](https://actionnetwork.org/event_campaigns/discussion-circles?link_id=18&can_id=6cd4a944ab0853240c109f7f3cff1830&source=email-monthly-newsletter-march-2024-3&email_referrer=email_2272210&email_subject=monthly-newsletter-april-2024-_&link_id=9&can_id=078080b221b070660325c17d0bc3ae19&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

#### EMMA GOLDMAN CLINIC FUND-A-THON

[![](https://actionnetwork.org/unlayer_files/1712799844563-EG+Fundathon.jpeg)](https://fund.nnaf.org/team/568876?link_id=10&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

Help Iowa City DSA raise money for the Emma Goldman Clinic! The Emma Goldman Clinic is the oldest independent abortion clinic in the nation.

[**Donate here!**](https://actionnetwork.org/emails/central-iowa-dsa-update-events-galore-2/manage?link_id=12&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

Upcoming Community Events
-------------------------

-   4/14: Join Des Moines BLM and the Sudanese Youth Committee for a [vigil for Sudan](https://www.instagram.com/p/C5eNShLOho1/?link_id=13&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore). 3 PM, Edna Griffin Park, Des Moines.
-   4/20: [Ceasefire demonstration](https://www.facebook.com/share/p/mzgsi5NKxCMRp3aT/?mibextid=K35XfP&link_id=14&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore). 1 PM, Cowles Commons, Des Moines.
-   4/22: Clean Up Mid-American campaign [Earth Day event](https://iowaipl.org/2024/04/03/earth-day-action-demand-clean-energy/?link_id=15&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore). 3:30 PM, St. Paul's Episcopal Cathedral, Des Moines.

DSA in the News
---------------

-   Louisville DSA and ATU Local 1447 [launched a campaign](https://ecosocialists.dsausa.org/campaign-leadership-qa-louisville-dsas-foundation-of-solidarity-with-atu/?link_id=16&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore) to reverse transit service cuts, improve transit service, and win better pay and benefits for transit workers. There's a lot we can learn from this campaign, and other DSA transit campaigns, as we [fight to protect public transit here in Des Moines](https://actionnetwork.org/forms/central-iowa-dsa-transit-wg-interest-form/?link_id=17&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore).
-   In state after state, the "Uncommitted" primary campaign is [blowing past expectations](https://jacobin.com/2024/04/uncommitted-movement-cease-fire-biden-election?link_id=18&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore). Combined with continuing street protests and direct action, it may be changing US policy toward Palestine for the first time in a generation.
-   DSA has joined a number of pro-Palestinian groups, including Jewish Voice for Peace and the US Campaign for Palestinian Rights, to form [our own PAC](https://www.axios.com/2024/03/11/aipac-democratic-primaries-israel-gaza-squad?link_id=19&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore) to defend DSA members of Congress against AIPAC's attempts to buy the 2024 election.

Finally, we'd like to ask you to renew your DSA dues today. Dues allow us to have protest signs professionally printed, reserve meeting spaces for chapter events, and much, much more.

[**Renew your dues today!**](https://dsausa.org/renew?link_id=21&can_id=078080b221b070660325c17d0bc3ae19&source=email-central-iowa-dsa-update-events-galore-2&email_referrer=&email_subject=central-iowa-dsa-update-events-galore)

If you have a question about something in this email, or anything at all, just reply to this email!

In solidarity,

Central Iowa DSA

