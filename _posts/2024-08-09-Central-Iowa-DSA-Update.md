---
layout: post
title: August 9th, Central Iowa DSA Update
subtitle: In this newsletter -- Capitalist Realism Is There No Alternative?, Gallery Browse Tonight, Workers' Round Table 
thumbnail-img: "https://actionnetwork.org/unlayer_files/1723216332896-82424+Workers+Round+Table.png"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---

![](https://actionnetwork.org/unlayer_files/1709856217672-Round+Logo+%281%29.png)

August Central Iowa DSA Update
==============================

"There are decades where nothing happens and there are weeks where decades happen."

Upcoming Chapter Events
=======================

#### Social: *Hurricane Season* Gallery Browse

![](https://actionnetwork.org/unlayer_files/1722634415132-Gallery+Browse.png)

Join us as we browse the Des Moines Art Center's newest exhibition, *Hurricane Season*. *Hurricane Season* explores the intersections of race, climate, and capitalism through the eyes of six Caribbean artists. It is an excellent coda to our most recent reading group, but anyone will find something to enjoy. Drop by the Art Center any time between 5 and 7 on Friday, August 9th to explore the exhibition with fellow DSA members. A social will follow.

#### Housing Working Group Meeting

![](https://actionnetwork.org/unlayer_files/1722634721496-Housing+Working+Group+65+%282%29.png)

We will continue our analysis of the big players in the Central Iowa housing market and make an important decision about a potential upcoming action. Don't miss it! We will meet at the Des Moines Central Library at 6 PM on Monday, August 12th.

#### Transit Working Group

[![](https://actionnetwork.org/unlayer_files/1722634937533-Central+Iowa+DSA+Transit+Working+Group+%281%29.png)](https://dsausa.us/SaveDART?link_id=0&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

The Des Moines City Council, with allies in private industry and conservative suburban city councils, is trying to privatize our public transit agency, potentially eliminating hundreds of good union jobs, drastically cutting service, and increasing our region's carbon footprint by megatons. At our next Transit Working Group meeting, we'll debrief our most recent action and plan next steps to defend, improve, and expand public transit service in Central Iowa. We will meet at the Des Moines Central Library at 6:00 PM on Wednesday, August 14th. Sign up using the link below for further updates!

[Join the Transit Working Group](https://dsausa.us/SaveDART?link_id=2&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

#### Workers' Round Table

![](https://actionnetwork.org/unlayer_files/1723216332896-82424+Workers+Round+Table.png)

It's time for another Workers' Round Table! This is proving to be one of our chapter's most popular events, so you definitely don't want to miss it! We provide an easy, low-stakes environment to get your feet wet with workplace organizing. Talk to first-timers and union presidents alike about issues in your workplace and come up with a plan to do something about it. We will meet at Ritual Café on Saturday, August 24th, at 10 AM.

#### Reading Group Meeting

[![](https://actionnetwork.org/unlayer_files/1722571020613-Reading+Group+Graphic.png)](https://dsausa.us/ReadingGroup?link_id=3&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

Our next reading group will meet at Beaverdale Books on Sunday, September 8th, at 6 PM. We haven't picked a book yet, so if you sign up before Wednesday, August 7th, we'll be sure to send you a ballot so you can help us decide what to read next!

[Sign up for the reading group today!](https://dsausa.us/ReadingGroup?link_id=5&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

#### General Chapter Meeting!

![](https://actionnetwork.org/unlayer_files/1722638691000-Screenshot+2024-08-02+at+5.45.07%E2%80%AFPM.png)

It's time! At long last, we're having our first general meeting. This is where Central Iowa DSA members come together to elect officers, make decisions about chapter priorities, form working groups, and much more. It's what makes DSA run! Only DSA members in good standing will be allowed to vote or nominate or serve as officers, so be sure to renew your dues using the link below if you need to.

Mark your calendars for Sunday, September 29th at 3 PM!

[Renew your dues here!](https://dsausa.org/renew?link_id=7&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

#### *No Money For Massacres* Email Campaign

[![](https://actionnetwork.org/unlayer_files/1719425717401-Untitled+design.png)](https://dsausa.us/NoRepression?link_id=8&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

The Iowa Democratic Party voted to introduce new, pro-Palestinian and anti-apartheid planks to the party platform at their recent convention. That's a good first step but, as we know, a platform isn't worth the paper its written on if elected Democrats refuse to abide by it. That's where you come in. If you haven't done so yet, be sure to use the button below to send a pre-filled email to your state legislators, as well as the leadership of the state Democratic Party, demanding that they stop using the law to repress the Palestine solidarity movement.

[Send your email today!](https://dsausa.us/NoRepression?link_id=10&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

Upcoming State and National Events
==================================

#### Cori Bush Phone Banks

![](https://actionnetwork.org/unlayer_files/1722637608374-CoriBushSTLDSA.png)

This weekend, help DSA member Cori Bush, one of the most vocally pro-Palestinian members of Congress, defend her seat against prosecutor and AIPAC candidate Wesley Bell. There will be virtual phone banks this **Sunday** and **Monday**. This is a great and easy way to meet DSA members from another chapter and help your socialist organization and the broader Palestine solidarity movement!

[Register for **Sunday's** phone bank](https://actionnetwork.org/events/cori-bush-phone-bank-with-dsa-6?link_id=12&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

[Register for **Monday's** phone bank](https://actionnetwork.org/events/cori-bush-phone-bank-with-dsa-5?link_id=14&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

#### YDSA Fall Kickoff

[![](https://actionnetwork.org/unlayer_files/1722637817241-GT6ck0GWEAAE6xX.jpeg)](https://actionnetwork.org/events/ydsa-2024-hard-launch-meeting-of-the-minds-state-of-the-org-national-call?link_id=15&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

Are you a high school or college student? Are you interested in starting a Young Democratic Socialists chapter at your school? This is the call for you! Hear from student activists from around the country on how we can bring the socialist movement into our schools. The virtual orientation will be on Monday, August 12th, at 7 PM. Register below!

[RSVP/Etc. Button (Optional)](https://actionnetwork.org/events/ydsa-2024-hard-launch-meeting-of-the-minds-state-of-the-org-national-call?link_id=17&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

#### Green New Deal Campaign Huddle

[![](https://actionnetwork.org/unlayer_files/1722638102800-GND.png)](https://actionnetwork.org/events/building-for-power-august-campaign-huddle?link_id=18&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

The traditional partisan alignment in the federal government is in flux in an election year. This presents new challenges, as well as new opportunities, for the Green New Deal. Attend the national Green New Deal committee's campaign huddle to learn more about how we can advocate for the Green New Deal at all levels of government--especially important when the ruling class is trying to end public transit in our city! The meeting will be on Wednesday, August 28th, at 7 PM. Register below!

[Register here](https://actionnetwork.org/events/building-for-power-august-campaign-huddle?link_id=20&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

#### Summer Organizing Workshops

![](https://actionnetwork.org/unlayer_files/1722638404584-Main_Logo_Circle_Large.png)

Are you new to organizing? Are you an experienced organizer interested in improving your skills? Check out our summer organizing workshops! These virtual training sessions will cover a range of essential organizing skills, and are open to people of all experience levels.

[Choose a session here!](https://actionnetwork.org/event_campaigns/dsa-summer-organizing-workshops?link_id=22&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

Upcoming Community Events
=========================

-   [8/5/24](https://www.instagram.com/p/C-GK_-lukvw/?igsh=MWE0a3NnbmtqZm5ycA==&link_id=23&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update): Join Des Moines BLM in telling the Des Moines City Council to reject the anti-homeless ordinance. 5 PM, DSM City Hall.
-   [8/10/24](https://iowacci.ourpowerbase.net/civicrm/event/info?reset=1&id=3217&enews&link_id=24&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update): Iowa CCI annual convention. 9 AM, FFA Enrichment Center, Ankeny.
-   [8/15/24](https://iowacci.ourpowerbase.net/civicrm/event/info?reset=1&id=3250&link_id=25&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update): Iowa CCI meeting to plan ways to pressure the city council to consider community input in hiring the next police chief.  While you're at it, sign their petition. 6 PM, 1201 Day St, DSM.
-   [Ongoing](https://actionnetwork.org/petitions/ensure-iowa-participates-in-summer-ebt-in-2025/?link_id=26&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update): Sign Iowa CCI's petition telling the governor to stop starving Iowa kids and participate in the 2025 Summer EBT program.

DSA Spotlight
=============

Central Iowa DSA is your local chapter of the Democratic Socialists of America, but one of our great strengths is that we're an national organization nearly 70,000 members strong. By pooling our resources, we're able to reach far more people, and make a much bigger impact, than we could in isolation.  This month, we're looking at [**DSA Labor**](https://labor.dsausa.org/?link_id=27&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update).

[![](https://actionnetwork.org/unlayer_files/1722638974107-dsa-labor-social.png)](https://labor.dsausa.org/?link_id=28&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)

"DSA Labor (the National Labor Commission or NLC) is a body of DSA members active in the labor movement, including union members and stewards, union staff, workers center activists, union officers, labor journalists, union retirees, students in labor solidarity groups, and labor-oriented intellectuals." Membership in DSA Labor is open to DSA members who are:

-   Active or retired union or workers center members
-   Union, workers center, or other workers rights organization staff
-   Involved in an active workplace organizing campaign
-   Journalists, academics, or other researchers whose work focuses primarily on work and/or the labor movement

In recent years, DSA Labor has been a major force in supporting the UAW's historic stand-up strike, the UPS Teamsters' contract campaign, the University of California workers' historic strike for academic freedom and an end to the genocide in Gaza, the Starbucks Workers United organizing campaign, and much, much more. DSA Labor meets quarterly. If you qualify, you can join them [**here**](https://labor.dsausa.org/?page_id=578&link_id=29&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update)! You can also follow them on [**Twitter**](https://x.com/DSA_Labor?link_id=30&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update) and [**Instagram**](https://www.instagram.com/dsa_labor/?link_id=31&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update).

DSA in the News
===============

-   A coalition of Western Massachusetts DSA chapters [picketed](https://www.wamc.org/news/2024-07-30/protesters-tell-harris-all-this-money-will-not-wash-the-blood-off-your-hands-outside-of-vps-pittsfield-fundraiser?link_id=32&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update) a Kamala Harris fundraiser on Tuesday to protest her role in the genocide in Gaza.
-   DSA member and New York state assemblyman Zohran Mamdani is [considering running for mayor of New York City](https://ny1.com/nyc/all-boroughs/politics/2024/07/17/zohran-mamdani--nyc-dsa--mayor?link_id=33&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update).
-   Members of DSA's socialist caucus in the New York State Assembly [wrote an op-ed](https://www.cityandstateny.com/opinion/2024/07/opinion-nys-climate-progress-failing-new-plan-public-power-can-fix-it/398385/?link_id=34&can_id=078080b221b070660325c17d0bc3ae19&source=email-august-update-26&email_referrer=&email_subject=august-update) calling out fossil-fuel-supported governor Lathy Hochul for slow-walking the Build Public Renewables Act, the largest initiative to increase public power reduction and build renewable capacity in the country.

In Solidarity,

Central Iowa DSA