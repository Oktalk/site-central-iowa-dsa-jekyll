---
layout: post
title: October 21th, Central Iowa DSA Update
subtitle: In this newsletter -- Transit Working Group Meeting, Housing Working Group Meeting, Reading Group, General Meeting Recap
thumbnail-img: "https://actionnetwork.org/unlayer_files/1729046226476-officer-elections.png"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---

![](https://ci3.googleusercontent.com/meips/ADKq_NYP7Rkehk_7vUOY9h7mmXNiqisy7tzk4PtlCt2iNMSkb6oBwmDa2KLjneCk4qxBENyNqpwepCKIIR0z-DbUr_fYVUz-VyK-ME0ME7vGOqCt67s1GVFu-EDdd6xIzPA=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1709856217672-Round+Logo+(1).png)

October Central Iowa DSA Update
===============================

In October, we'll be focusing on strengthening our working groups' engagement. There is lots to do from taking action on housing to transit and to our upcoming chapter officer elections. Read on for details!

Upcoming Chapter Events
=======================

#### Transit Working Group Meeting

[![](https://ci3.googleusercontent.com/meips/ADKq_Nak0iOpjSgKTxqJdiX6WIj4fwDQq2QpoT3PxrHf1uK42cQIwobVWzohDGnLDg1S-ytfiS0wvtaWBuFbQKcs5FTzr2L8yH7lVxB4nFdjDpjksipIMMX9CATY5g7_2ZDpRI244JNX=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1729045769550-Transit+Working+Group.png)](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn31Yk0b-hEgkytImb9sCZKh95MU-IMWhNCJrta3w_12y0W83pTZKButfaxxPecTA8bsFZaGHbOhQn-JrNBmDIhETIYZI2gRygyVZjLF55Zlss3rbbslIgAIw2P4kLMzCW3KMqBvDDYew9ybidJvnJjkJ_Xjw0JKqpNE0-LT5zJlfWZ1uDblB2w6jtsIEte1wkF0M9HI68eFqArq6TLxKdkXOnwutnnkh_1XeD7sCZCJOw/4as/SIHA32eLQx2qXS1Cj1RhtA/h0/h001.sJI6eEwcWMZlj7wIC4GKQXy9BVg-hr9rwNY17V-G6lg)

The Transit Working Group will meet on Wednesday, October 30th at 6:00 PM at the Central Library (1000 Grand Ave, Des Moines). The Transit Working Group is fighting to save DART, Central Iowa's public transit agency, from catastrophic service cuts and privatization. Come to the meeting to see how you can help lead the fight!

[Sign up for Transit email alerts here](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn31Yk0b-hEgkytImb9sCZKh95MU-IMWhNCJrta3w_12y2dvoc971m8M4Fqrbs5cpChFT90qwqIc4fvAz-qLfm0LAUTIT7Kf3Tr0FKuW2ON7HQtHrIf_3uI-J8C4Wk8C7gOFENxA41MiLDS8pjB5VHOwjxi9CYeYJ4Dhc4v_WEFIPBEnfCCZ9keKPyLRG5KQssq_nCjZmnp8y3z-MJ7ByvzBX_RHo_x5FiUOz3Qtg1tQ2c/4as/SIHA32eLQx2qXS1Cj1RhtA/h1/h001.7o89BXijlv3UoSNnYnffrPmM_2J09mUddMgh2Yo_BOE)

#### Housing Working Group Meeting

[![](https://ci3.googleusercontent.com/meips/ADKq_Nbg4zYDnDaTjdag_VyrGslab6K_wFkaiioXdJ6WyXAG1Xj9OW4kdNQn_kacOcUrrmW7An3OBckXMOLVU8jTPHv6tEpFK4mXCuXaaIOnbixvnlbiuc2_jQ=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1729045828817-housing.png)](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38ZOwQTDGAMTUsrYrcgK3c6u1WvswrVOXdYNY1lD_gERCotsTWmKJQxbGQhymlGPghbc86pw2EWr3qBFWxr0VRdWi1znv-yK-cqMam36EGQ4BlSxghywBGrKDKoinbYaCEsvrw_-2KwkFJgYQs5v66GkdeTU_jML8Wep4c_p1mK6_IhRzHPedzKwqrMrMRf6zJKzfXMuKzfQgbWzh2GcgeAJYSn5-aG9rfxWaiPL7gu17msHc_S2C00UiWuFe2cnWA/4as/SIHA32eLQx2qXS1Cj1RhtA/h2/h001.9RpZZC3AzRvvefGP6fi2FyxnNxLUOwi32na9jHEGIgE)

The Housing Working Group will meet on Monday, October 21st at 6:00PM at the Central Library (1000 Grand Ave, Des Moines). Agenda includes discussions on a new canvass plan, potential locations, and more. If you're interested in helping us take action on housing issues, come by and join us!

[Sign up for Housing email alerts here](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38ZOwQTDGAMTUsrYrcgK3c6u1WvswrVOXdYNY1lD_gERCotsTWmKJQxbGQhymlGPgjc54bgNeSiRNFzCRxnArbqytc9tRqDHuGscH5_REChDIIkUMcl9E8XniGz27q_02x4izXu_TEDcnBWBYFzI47YkbnPekiY8OomogQksJ9_3Db7d69ndEEq0KJKe-fWG33T7S1NhiSeQV-es8EaB6vzB2cnbTemJSjvn9vosUIMgUlZh4Lsa_HP_NpayeM2fnA/4as/SIHA32eLQx2qXS1Cj1RhtA/h3/h001.nrZZwWMAeOjN4QdrDGEIsZ9X4g-wcd2yXhcqUziPtes)

#### Reading Group

[![](https://ci3.googleusercontent.com/meips/ADKq_NYfi4FjlAUW7Xz7izfci-n8Cd6SYSzOkFyJ5JTdRIa8FNE71x4H5J05bT8wm-jzhGEHgt2v5V01j1eG7XNsYSzGQccdciyr9psI4DJECyehaDPZhFVNIZdX6tEE6YB4oWR4u1sQ=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1722571020613-Reading+Group+Graphic.png)](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn3mZX4PcxQeE4-feRa-3YstGqHaTveluH27tM5H4BhR0qQYtCuqqHOUmpt50gszapxGCoHz3SJqJynQQGetW9W4RSEpN4ATdMJEnUqDd3hTE5PxpJZGo6kI726U4dLWMqmzTheFLcqDvjwV-lv8RtFKrUzuU4XJOaNxt8WgIwyfvvLwr6NHVJnqquAXgHS2GENg63rd7cguzgw3lG_DM0t2PZisGrE1xcWV2CTWJjz3Ik/4as/SIHA32eLQx2qXS1Cj1RhtA/h4/h001.Xcfk85f6y54xaJkGm9xi7XOoklNRvf4XjVd7QGHAkLM)

The Reading Group will meet at 6:00 PM on Sunday, October 27th at Star Bar (2811 Ingersoll, DSM). We will be discussing three shorter readings:

-   Nancy Fraser: "[From Redistribution to Recognition](https://url1005.email.actionnetwork.org/ss/c/u001.HV-T5b9C6Y6Pisqo-P4PmyreXsa4mae8wI_MCdUHSXLqzv8bdPiheT_fUlwqArm2syXhJcqOXo3r4KkW03WXlEkQUR1afdwZIjUMPcq55I9gKxg_gRmmZzqcqwpEXo_AQkhzU-ub1Qv2iY0CxqAd6VSWdIB4fClr_frOpgs-MvdzoJigyJju6qwCwn8k9aGFKHfMryPljsGl-OV_qfaUMVUYQxXW0isFABv6e-_clN-NRQjHsekXcgL5jmzDAhAUa72uQgcMp6rfBg4GUs-W6g/4as/SIHA32eLQx2qXS1Cj1RhtA/h5/h001.Z5yrv6kk1iBvT4VHPuULSnLM3idqnBdl-WtseUOhk24)"
-   Bhaskar Sunkara: "[Free the Land: An Interview with Chokwe Lumumba](https://url1005.email.actionnetwork.org/ss/c/u001.o1U7tAOlmzfzv5xjBjF-eynRc9ytF-Zu5idr9QkFWObfsNmKooQrBVe4DxVb2tW0wXuh4VEGNV2sIeijSyJPvpOugYsXraC9ayxUiVcEWribWm-rMiIxKTERmgkPMdQZY7GcFNSnhczC3Y1CJzynEOmRLeJaBpGDxf_p2F4S72MujE7IPMAXaCpaF7BkLEJZixiNH0hBtAyZMuIA9Op4ig2JLhq9mQamjejsWqH3VkO1pIzWCE1-pBmKRI7RkU4H/4as/SIHA32eLQx2qXS1Cj1RhtA/h6/h001.p0Ey53FvlmBTl18Ax0l4TgWoOq0Js7r71VGdSsX3ZAE)"
-   Vivek Chibber: "[Why the Working Class?](https://url1005.email.actionnetwork.org/ss/c/u001.o1U7tAOlmzfzv5xjBjF-e2oN0VOlkJ3HGPx835hecR8Uj3fWWYxuq872-CsS82GcX1JkVsfDiIaV1sBBP3y_b87s4GGZuWlt2MU3IkwDuMdGFxvEwAt3TlHNWbem_FllWSdjhbxsNGUvfcrH-oVcilha8VVHtaEuhUvONyJICXsrJApROuDxZtzD7gtP-5TlVdl3NwJbChUb8zHfZSMo3t7UXQ06K8ItuPyOfYIDkTZMht24NxM1iTn7x1lNbQ5hTBi9Us9ATwucxnVYWoaaHgvSWZUvbPGZjr6ANlR6eOQ/4as/SIHA32eLQx2qXS1Cj1RhtA/h7/h001.-OikJLv9upJLgvz5KX0DvpUR103jY3y9IPkUbrCXlpI)"

[Sign up for Reading Group updates here](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn3mZX4PcxQeE4-feRa-3YstGqHaTveluH27tM5H4BhR0qnW69KTscs_sadp4dG7RhopKtghLhJJrjfgH5fyBbWiBbOGF0RtlLboRQ0poR7d-klzaEjt3li4Gvdc3lGfu5riQVVDeppNXd2WtwoAxT4nL4oi2Bf1jGK3rNqVGIWRQTLh51DlS5P1808AuyjlFtztt_GaNzQ4FbTXocdkdHobFaOuOzSpr8F0SKKlByen9A/4as/SIHA32eLQx2qXS1Cj1RhtA/h8/h001.BMf7zSuGFue5zOpd5z848kc4LZkM2jvMHq1FPEU7Ms0)

Upcoming State and National Events
==================================

-   10/29 NPEC Capital Reading Group #1 Parts I + II at 7:30: [Sign up here](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm3-X1Uu8BwoVNK63uNQqT4rBu0-4MdsDjxuuCsRygpv9Cu_KlqYpB_0dglhx2bi7RmILkOSazsbTK6F6QciIQbdb22vOXWsUgssjOA5T5ClKgMUIUNubbiXX6dEVxdvDvCG-rokxvpn2n4L9kaAyvyKUuJgarOahkkqp7bQdq0esWqDCzhFu2IjBbF0nAgvOyLGXQ1431-eZ-1OVldtVD0e5z7AMTrMRisA7ogcSfmVqTw/4as/SIHA32eLQx2qXS1Cj1RhtA/h9/h001.QKFhpvwfRVYbEGk4oKGHXLPafKVnvL61OyQRw6Fz8dU).

-   10/29 Venezuela's Solidarity with Palestine at 8:00: [Attend this event](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm3-PPim9spkqAQN1pC4iz2aF75xtTtTFg0NrR9yf-H70Thh3VyeVyUsFKIDlaXbTfZaqQcZ0hjDwHYlW0G_x3hv9L1o-cjL3Tfy0kTSQKW3rUq-Fs1zClafsHyIOWKr8spHv14VruIecCYOeFd-RuaLI5YpnbFvsD8DXC9MfcOrUkYurGB-6AJ0hUUFhN09iu1-JIdviDIUCJRGQXKILSQv2TPWTIfOXxAqOUM-NoX2OnkKLeOsv2D8LRXaNzZXHr_4/4as/SIHA32eLQx2qXS1Cj1RhtA/h10/h001.k2qFTPlp8L9HaCN6oGpE9fx2le5WSuO1K9G4gJpMFSg).

-   Striketober 4 - Solidarity Captains' Meeting: [RSVP here](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm39e_hf2kBDAbn3aAVjKOwNQ8k5rHgcmZS-RoPC4wYQRyuB_zbtnS-S1gdZiPblc0ITm3GTAnaL3QHsmW6-GgeU4dTDIykgYQfIlhPpbi0ZJxm5EKb4IjA8dpKuVQMGSmYbWeYA6MgKFLL2MjHeQhV4XooeCUf6VQ1G6Toldqt-AnuQNA4zch_DylJehLWNn5HSNNKcuOr34bhANUnLFEMUXK6j5bPyoyd_Tf5O8OpaFmPeRVWyCT7Klejv8wDN0C88/4as/SIHA32eLQx2qXS1Cj1RhtA/h11/h001.q6m9zSNCiBSQZL-9uKPLIeTgOc4HQlL5H2GvBkiPg1k).

DSA Spotlight
=============

#### **General Meeting Recap**

Last month, we held our first general meeting since 2020. Topics included updates from working groups and a discussion on chapter priorities. **The next general meeting will be on Saturday, October 26th at 2:00 PM at the CCI office (2001 Forest Ave, DSM).**

[Read the meeting minutes here.](https://url1005.email.actionnetwork.org/ss/c/u001.yjJN_MEbNx94PLaWzZ6hW451kB1UHFkGz1cupG1HPlLcvgz6zJUfIOab_wJL717dvk5kBqE53M6uzjy1pHPuGYqy_rIN_e02caqfbmvM2I0uL_dQbSELidIPOqf5ijqQa5z8sbIQcTt3fYlQ8Fs1L31bBeqOz3lhDBUfxIJa_xVmL5AVO2rMYD43Swh4CI_SWixNqq4QbN6qK_R9nqAD76lLkE0G78wipViApS9Wtforo2b-zj50bvErdeUClwv6K4bF0PkFLoOAmmVyJbHEdVXoPF4JKKeSubHaX6-7usSjjwiKvLzHMXZy9MN8pMEE6zN4mCpq53IDZDccp1HIXw/4as/SIHA32eLQx2qXS1Cj1RhtA/h12/h001.zk8KD2uIgdDXX1NWX_yX3Q98FIE6K3KEk_kdg6KMvns)

#### **Officer Elections**

![](https://ci3.googleusercontent.com/meips/ADKq_NaTyBhydpvYGsiK6kDOXPJEuXnsa8EhZOBacTwdXrpcxOII_VOS0DdBdhlqQr4QQQ1QE8wA5QPxQ95_0AQLn_yX4WI8zHH0iHHVLqzwc0J4Ax8rNvGkCDPchTOuK9NQgKY=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1729046226476-officer-elections.png)

It's election time for chapter officers! We'll be holding officer elections soon to select our new leadership team. This is your opportunity to get involved in our chapter's decision-making. **Watch your inbox for the official ballot!**

[Read the candidate statements here](https://url1005.email.actionnetwork.org/ss/c/u001.SNYHID0cgDJeR88MOfSMuagpNTDg5_llL4UBGSZ4p85QSHk8yP0KFkQGwu53RaSytmoOzdEW9Ec5qPoV5SRAT0ozQ0a9XbMlwaDcoYSGcsE7XklY7hk7_NQEp8jToZM4cJKpN8_RPG32boACuS0dXDV9tQORAOhTNscajz4_chhJGe9TgtZ6pj3ND-edq6zeeGANDTVjtnuifCzRYRiuYPM0obUCnbhSr-j77CzbZUQ3bG0v6pwJ1663fE-QeLZi/4as/SIHA32eLQx2qXS1Cj1RhtA/h13/h001.TjwNp0OjE4Mr85Uxw05iSop8vDoy55Q1Xka-EoW9j1Q)

#### **Get Involved with a Working Group**

We have several active working groups, each focusing on key areas of our chapter's work. Fill out the forms below if you're interested in joining:

-   Communications: [Form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn3JO_kglQWrpmYeyKYW-6vJhq1zA16AARRHg4Z9Gw-ACYafQoVJ3W-7pSatRI-o9-uGN_hCEAXuFOzAAUE55KGU9bTAN8qMh1tWzLLdFPyLjfasUkoxFdJ80uoJeXGjUpVjO595PEkYJ4kLGAMGtvOIdAIHkkcJuiw6XD27ZSv9IYeetOm6SAyzXpvbsvzlj6vJn46iDliwjJ9CyT6ZBwNCV-RxIn-KV7tI3rQAiKdsrHlZEwkLfx5HuKci6AWF2Pa/4as/SIHA32eLQx2qXS1Cj1RhtA/h14/h001.bCbMpheJX438wI6-d1BD2oJXmIbiOA8kb3OYC2r1KQg)
-   Political Education: [Form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn3H4Lew1vbGnksn2a3K2hm1Jr06nBxDG6tZbv2nn6bOzV2NzCwycUeAosEfAjuApVNgzjtdJ-9je6wsebPj_30DiWpyodVWDxHjNKtIQPo3iI9L9ZvBl-6o-B1R3AuYr7IqU22jya02NEY8Cj_0ImqFzAvWUy2ATD9E9vs_aZZ-sBnvLeGsm8vTuwsTt_mFSL9jfgKX4XdJ3zqk4wGrqp6Uq5eDBo1Mc3-xUI64xjKFG6rqzE0zUqJW1yVs_uPZYbx/4as/SIHA32eLQx2qXS1Cj1RhtA/h15/h001.nikBzh1wVhZwvHvDnE_5pzXJWgJYI2Sy1-nJEKfh94s)
-   International: [Form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn30gFC0wLDWvAImwZRm_3IxvZOBSf6FKkz4WU5ThjtJz4pdNtmVgdf1tBjNxvByxtHfiFkS6dNpwP6boVrmfoSDl4M1663RV0B9jsJEYdq6AFEl8MOVn3UsGjq95rQDuU2yoCJDAM-QJR27CLrxLFGWTFFrSyHpyMvGxY2_YWe0UAdPZsM5XLjxXiRiA6l25fjI1wxPoHnMmNslARWdKc6O18-WI5z4xYm1ZhG6g2U7TqQ3EGgUnt7rVA4udCwqxfe/4as/SIHA32eLQx2qXS1Cj1RhtA/h16/h001.nJ0gtWG0GzjxlqHtr_FXGayP-I1uq4g8hf0Fww06khs)
-   Housing: [Form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38ZOwQTDGAMTUsrYrcgK3c6u1WvswrVOXdYNY1lD_gERCotsTWmKJQxbGQhymlGPgjH3g9EZxUyFxBApy6eUBq0WxGb4OE56q7taACdwsngqHwCPG_pPTnH0x7q40MSLDB5czWJv6rRswjbR6opIApzLFQFV2rOQn2io0WA2KFzPykF058tMQQFMGkkEXQKTmfr7mkARz30ZaboT_Wvuu0msYB8fpkRw8S_ihZC2-qxNgsp0FFhUhMDKZKzo4eAW3A/4as/SIHA32eLQx2qXS1Cj1RhtA/h17/h001.CrqF0X7EVME8aVsAWvTH2zJS-kb5N2pwWOAzwUk2j-E)
-   Transit: [Form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn31Yk0b-hEgkytImb9sCZKh95MU-IMWhNCJrta3w_12y3XaY8hJE29izkms1zF9tBJZE9JjpCbw3qfDActU2oNaGT2GWkOgAH9Exv7hDkWpzZlReAxp_Ra5vdFHcp436rUsNSHQwK3PDArfodlSzvydv8lGieMVTjv_UOUqig2byMo0xV25D1uYS_yHo4hHvW7qJWKQFZ3MQCW0BZsyGJa_AxegmY91-HiXVAT3FpbRPk/4as/SIHA32eLQx2qXS1Cj1RhtA/h18/h001.4FyQhQ3DDQKGQNXAZlqcisqM3MzfgXq9hC0Uw-jaxmE)

DSA in the News
===============

#### Transit Outreach at Heart of Des Moines Farmers Market

![](https://ci3.googleusercontent.com/meips/ADKq_Nb_mc28CeyIOZNjNjjKkGg7XwgNzd4ODA7f35buIePl8dLER9FQJxwSdVqo2qgMkg1vgea-HJWY3kz_ga8O8jjXNu0SWvZJnpBeh8wcx9FhNekyd_OCVGUO6mmMYhkeJrDxvzSlq08XlyBKltlkzJTz4WuuuoHCBJ2kZfhLtr35jg=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1729047536652-463075309_17884306212105425_3298705531806084114_n.jpg)

Thank you for all who helped in our tabling event last Saturday, October 12th at the Heart of Des Moines Farmers Market. We talked about the importance of improved transit services and how we all can advocate for better public transit within our city.

In solidarity,

Central Iowa DSA