---
layout: post
title: February 3th, Central Iowa DSA Update
subtitle: In this newsletter -- Transit Working Group Meeting, Housing Working Group Meeting, Reading Group, General Meeting Recap
thumbnail-img: "https://ci3.googleusercontent.com/meips/ADKq_NZLD5xd-8ScX3qqwDI3ElpBmtUcGn7dUl2eQDAlfRgyGi5rkIpax2Jn0zl4XDk_v9V_hx_LIjMDdKKUvBfVkEOiaVakmzbK4Qt54fyY_VEC3-7F9obx92WCY5xKv5ID=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738595417925-General+meeting.png"
tags: [newsletter]
comments: false
author: Central Iowa DSA
---

February 3, 2025 Central Iowa DSA Update
=========================================

Welcome to our latest newsletter! In this issue, we'll recap our recent General Meeting, then look ahead to our next gathering. You'll also find updates from our Working Groups---covering labor, housing, transit, international issues, political education, and communications---along with details on city budget organizing, and job opportunities within DSA. Read on to see how you can get involved and help shape our collective work for a more just and equitable Central Iowa.

Each monthly newsletter will be available on our website, [centraliowadsa.org](https://url1005.email.actionnetwork.org/ss/c/u001.EtNV8HBC60Tl7UuGmXS3sQm-WJ-LaLiaI5IaLRkZTDDJV2WvZQIW5PrUOIQbwvFMM8gpaRuuhpbDcY9wVCCHIJ-fS_g1H9B4yLSbAdLVwRO3kn4KzMifOGpSuS88UWFCm_aFd-URDxVSRmytuyoAjeW7kbTPP1ukp2rgIN84KVJLQsUilSq98TCWzQzedwwF1JD0YIqcKRbk7Y_YdS4s4Bxu97iG5XblvVArtKNdHAIMQsBaMpr5P8TEVCwtU15-jbk6kdk0rTL3jx3IGFLFfKkDbu_6BYcjlODxHH-AJjy1Z-j7H6-5-hHWxheQRt6H/4dp/un5WJP1US8uxbaj_G7QlVQ/h0/h001.O8ZPtgrRTeEq7vrsWW1qZfbvwcoaktMmy8b7_JaSnr0).

Upcoming Chapter Events
=======================

#### General Meeting - 2nd Sunday at 4pm

![](https://ci3.googleusercontent.com/meips/ADKq_NZLD5xd-8ScX3qqwDI3ElpBmtUcGn7dUl2eQDAlfRgyGi5rkIpax2Jn0zl4XDk_v9V_hx_LIjMDdKKUvBfVkEOiaVakmzbK4Qt54fyY_VEC3-7F9obx92WCY5xKv5ID=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738595417925-General+meeting.png)

Our next general meeting will be on **Sunday, February 9 at CCI at 4 PM**. DSA general meetings are a space for members and newcomers to connect, learn, and take action on issues that matter locally and nationally. Join us to collaborate on campaigns, hear committee updates, and shape the future of our organizing efforts!

[RSVP Now](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm39MIZWM1I6LENOJIS85nU_8E-i2uyxUwbQCJ_sK3e5KnIb_c_kB1dCxohusTaXTTLRCBBEBkU6g86DdVZFfjXp9eKbz7iTnrHUduvyIseBeaHA5rV4One0Tu2CYAbbvNTFQWPPwIOgms628LbFVTPmbo7zJmShrBxQMV_U9JGEB5H2zV1meXxkc2-1Nbx2IcN6wcoQ21Mv-NczXNXlKG93NaUwDAg7ORnr_IfmYbPPacOWtb9f2r8s-mBRArCpX87quJeyXAnzKNDgkBP4EuHpsP_nO6IQ--Jz5lFOSNgyBgCCrUPUVf1AvApDk7F2d-EfbE2e_Ku-oQBsE9_egD_6K/4dp/un5WJP1US8uxbaj_G7QlVQ/h1/h001.y1ygYNsQAvsKebejd1UjZjDUY8YVLULfVRRC41Nx-1M)

#### Event Highlight - Union Screening

![](https://ci3.googleusercontent.com/meips/ADKq_NZC4l55DXIEoDPrX4yPkF69rB_fZ4IdLm27HkNVkxSifPsSejmT9IoMbU9WZNIVVmfnaXsiUa13g2oHfYp8S_6A23sv8zraKIsbi2M_O7v9gi5jXBLRXKs=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738600589516-IMG_6032.jpg)

The Political Education Committee hosted a powerful screening of Union, a documentary about Amazon workers' historic fight to unionize against one of the world's largest corporations. Held at The Garden, the event sparked meaningful discussions about collective action and worker solidarity.\
Thank you to everyone who attended and participated. If you missed it, you can still [watch the documentary through their website](https://url1005.email.actionnetwork.org/ss/c/u001.XaF8mXqsA6b2dSPmhsleMQ8A3X07m0edlsVMfx0vOriCpcO7Xhxe8mc6kuBoWQXtNjXdwF8YB0hlyGWC_lWfJpTWs06xK2bP3vy3ybUmVsOjluTC3S79w29j7w2KOdp5MTWbk1JUkcO91D8_SAWShsDpUBwilhmTC4-7-P_RWW6CanLpOuWR-siIHj6nNAy2Z3ZNmxS5Zn4fwvg0pDAsJ7zWwAluiqzhtyVRdHaYcxQJa4tGWcV5NSdNGEzbFpcmqQPXRasP7WaNa9hhFa_Nzfo8MAiRj-_s5GI5geZw32Qpj8EA-cXB9s1DHKZ-jwJA/4dp/un5WJP1US8uxbaj_G7QlVQ/h2/h001.2PMXFkGz4Kujpj7K7HFX2LBQFNPnh4wnex3Is2gtMHY). Stay tuned for more opportunities to learn and organize together!

#### Event Highlight - Advocating for Public Transit at Drake University

Earlier this week, CIDSA members connected with returning Drake University students at our table in the Olmsted Center. Throughout the day, we had meaningful conversations about the political threats facing public transit in Des Moines and the urgent need to organize for better service.\
Thank you to everyone who volunteered their time to engage with students, faculty, and staff---many of whom shared their experiences as daily bus riders. Together, we're building momentum for a stronger, more equitable transit system that serves everyone. Let's keep the movement growing!

Working Groups
==============

#### Labor

![](https://ci3.googleusercontent.com/meips/ADKq_NZBHfmEIo_ij2zotc1Dp0D3q8c4mFXznDht2j3Kgn5RiPzOrvWnynX_Mml8me3ZD723GnRzyE2EtVhSpQsNSebmWGKNt6jMFhNihl5VKFQ0rckI1FQ=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738210990983-89074.png)

The American oligarchy is hoping to strip away the few protections workers have, and we'll be discussing ways to fight back. Throughout February, DSA members will have the opportunity to show their solidarity with Starbucks baristas, railroad workers, and local Teamsters. If you are interested in the labor working group fill out this [interest form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm39MIZWM1I6LENOJIS85nU_8TPWRz2tJ68JpJS7KEVowGcflp0FCfIeJwks822vMBXkZzhdD2pWXLsArMENeObJyTCewEEIEFOeJJGXCCryggUGpLHFQFmXQh9RDPEoSeW617hohb0N3iHZk4SyWsMkmQpdmkmpDnsg3NYyfcgSmg6bQ9Uw4kx51E-0noFqZGbpqyj6dj9oghqZ1bSD5rAnASHRypWCbdIeuwikGxGrMrjT7kNugP0gtONgCEtJtFqXzbDOXu_6bXtcNijZ8OUqqy-hQZeeSvDLppgdNCfTuw-rHn_iGVDlutxKm5Qv2lNDjeISqL3u5nWwHkoItuffs/4dp/un5WJP1US8uxbaj_G7QlVQ/h3/h001.-wHcLan-S7Zksw0LcR2FhRk_Qg2dhuU3NCQbmXVwfHk).

**Upcoming Meetings**

-   2/4 - Labor Working Group Meeting at Central Library at 6:00 PM
-   2/8 - Starbucks Workers United phone bank

#### Housing

![](https://ci3.googleusercontent.com/meips/ADKq_NZrbrjVvXD9UpM3a3y6C1lzXHKs1Z9Vtv0CFkRG23T6UUPWlpSTntqgH8QuB6hvytFsLw61mcVGchXtVsqyRvd8GGrnO7osiEJnBTdxU2nImg_XEPPB=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738210897114-809778.png)

The housing working group is dedicated to organizing tenants, advocating for equitable housing, and pushing for increased public housing through city budget efforts.  Plans are in the works for a housing canvas coming up, if you would like to participate in the planning, now is a great time to [get involved](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38ZOwQTDGAMTUsrYrcgK3c6u1WvswrVOXdYNY1lD_gERCotsTWmKJQxbGQhymlGPgjc54bgNeSiRNFzCRxnArbqytc9tRqDHuGscH5_REChDIIkUMcl9E8XniGz27q_02xmDHPYIbogsg-5w9emrC3ULxkKOY48JBCzJnx1GSfgm40b1_Rhih9D4TqCoeM9UkSc_xLquahDchVhh8d8hZ9EEarnHAOEZULKmYZ8vP65aJKiSnkZs0_ukUt2kBnZ3Xi-wrEqXk2wD7xXzhps_KezpCYkrC3THEOTHQwmOLy6mowOAS5JeNxAxnHzkd1vGvE/4dp/un5WJP1US8uxbaj_G7QlVQ/h4/h001.rjgxUIZriCKl0G7Mcmm7q1Xc_YozCFh33trWnKKdDn0).\
**Upcoming Meetings**

-   2/10 - Housing Working Group meeting at DSM Central Library at 6:00 PM

-   2/24 - Housing Working Group meeting at DSM Central Library at 6:00 PM

#### Transit

The Transit Working Group fights to expand and fund public transit in Central Iowa, opposing privatization and advocating for a stronger, fully funded DART system. If you'd like to join the fight for public transit service, [fill out this interest form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn31Yk0b-hEgkytImb9sCZKh95MU-IMWhNCJrta3w_12y2cVYoID0cU5d7zZemwNXplYa5-BuLxFWHRp5RlHzyk6lMlthZ78J0mBUZpAFe4yrIBPG6XS04w2o8GgyQCQoQtlLVvNQDqsnnO7Q7k_32ItsDIOdWbjpiv5_94ttC39An8U8CU7vCHfPS5_Se61VpDvEar9xIBX6VtqIrODfQHMzcwQe1rnsqyjTP-GKeRr0D-LZnTT5WB8Q3ZWzc8I35eFuTI7zXjxncH_Of1NZrYe1NybqrK7of2dj_71IJ_uGRpqXpXrs9AGmoB2m2lISDe/4dp/un5WJP1US8uxbaj_G7QlVQ/h5/h001.vg6jF_c6F4AaSjtXIuk_0xbFD5V23Ow_f15J21ob-T0).

![](https://ci3.googleusercontent.com/meips/ADKq_Nao3eZkmMcUduYWeibXd08ySAMQi59_0DLzzBcGTjEM5_R88v0igDlX_rVMrjC2EtEPJEohdRT9A6vJNwBdfXXrezGvWc8Qt1bRLdtNQ2iq89QtX20n=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738210618994-image1.png)

On January 11th the transit working group canvased businesses in the East Village and asked them to place a pro-transit sign in their windows. We were able to post roughly 39 posters and had good conversations with workers (especially at Allspice) about how important the transit is to them.

On January 29th, the working group tabled at Drake University, collecting dozens of petition signatures for more transit funding and recruiting several students into the fight for public transit! 

**Upcoming Meetings**

-   2/5 - Transit WG meeting at DSM Central Library at 6:00 PM
-   2/15 - Ingersoll Business Canvas at TBD at 6:00 PM
-   2/19 - Transit WG meeting at DSM Central Library at 6:00 PM

#### International

![](https://ci3.googleusercontent.com/meips/ADKq_NY40hmoxJlrFkJL-X0ntSWqCNfr6ijvZYocDHooCKy_QGUHszXMWo30rjKkfBIp9Zv0809ShWoW22oMzgYWnJb9n13DD3MD_Z4tmh9PXfTUweKLcLli=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738210755790-image2.png)

The international working group of Central Iowa DSA focuses on promoting global solidarity, raising awareness about American imperialism, and fostering connections with other organizations to educate and engage the local community.

Fill out this [interest form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn30gFC0wLDWvAImwZRm_3IxvZOBSf6FKkz4WU5ThjtJz4pdNtmVgdf1tBjNxvByxtH5ASV_rh1-4cEh7BlS6MVThF4eZSZNUse4JqCYrNUrdmW_tbSMU08nCnb7W3OaqENAjE2zXXuqMe7ri69HZcITj4e-KgO-y28mwIvnR1mAF2h8R5iOz9kS-iSEKEliuh8UMbf2kFTcqAk6wmP0S3kCKGOP4Cl5qo0Yq4_4vHTyEe_z86gjuEtUe03u13vOCpi6DIfAbDlfolg6SQBmt2CNT5hbmHltENiQmcPzolOlfIekCEAJfc-Oauk2bgoaKWdugzl5GXGg2zsrEiTT3XzMw/4dp/un5WJP1US8uxbaj_G7QlVQ/h6/h001.ejHzviC-aWAnxnAzKi0nkhm7DweVCv60mKr8bQbGxtM) for more information.

**Upcoming Meetings**

-   2/18 - International WG meeting over Zoom at 6:30 PM 

#### Political Education

![](https://ci3.googleusercontent.com/meips/ADKq_NbQtKRL9NQKC2j_LCjhZLE7tVuROiLBZ0dtcKqJxIRjgUp6FWCOm9YhYLEZC87ZVeOzy103oVDlsyAdE69IKBGrS7c7jAFj4ueldhveJ6EI0H9-tQ0=s0-d-e1-ft#https://actionnetwork.org/unlayer_files/1738211224401-82752.png)

The Political Education Committee fosters growth and dialogue within the socialist movement through reading groups, social events, and events like Socialist Night School, which explores fundamental topics like capitalism and the working class.\
[Fill out this interest form](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm38yK4Pzdyrw8B4bYfEK3Mn3H4Lew1vbGnksn2a3K2hm1Jr06nBxDG6tZbv2nn6bOzV2NzCwycUeAosEfAjuApVNXiP26fArBNURThidzZe9JSc0I4nIk9N6e480-ouKOTrnJqU-SmPkS4BlWZIKzWZPvUaBMXYNTK-cxyZd7sSoSeLZU3L-DUVXMaOoI3mmXdOzjUP5C4SDB6qG6msHNS25_6b313LvtFAjbilHq1P57ZkbI4abY7FIYwfhPc4Xq_zZa5gumaB6riV30NjAH_Y-4mErQr4J0FGzmojN-YSAKFfD1HdoL3meIRzy-6NjTZcK2BfgIaGvKYLoELo7upBQkgH6SVEmklhYwPsH8kTYww/4dp/un5WJP1US8uxbaj_G7QlVQ/h7/h001.R2PbdkdK-l2zqs_CDVPUBQUzQAxfFErYy2wedYQJP_k) for more information.

**Upcoming Meetings**

-   2/2 - 1:1 Training held at Gravitate and hybrid via Zoom at 5:00 PM
-   2/16 - Reading Group at TBD at 5:00 PM [RSVP Now](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm39MIZWM1I6LENOJIS85nU_81brFX781fKKnI4_ySHGefyJ69kUDoByjUYMYJ5m-57bRSmNN-dQeqfmaAJFQccphJHtBBJP5hK4ciiR52LUA3ARlRVnKNu19b88Aj7CSDarpbOr3B9IydJ4CdruUdaEAtypjyj-rTjZklIwrlYdRpRV-nr71m6PHgzB40kXffXftJ54YQQl1FeetMNxpkWClPj3R8BGVZvKLfy3G3Eg6MiGK7juyPQ2nOuhJp-TNrJDqsMMNiysLXyRIApumSMcMZJ62Tq1-EknE5mxFze83vt4BK7bOwLiAI3JiFYa8O7eLx5MIwVc2UTA78tjfHjmg/4dp/un5WJP1US8uxbaj_G7QlVQ/h8/h001.ohhRGR8EQRLMCD8QcQBmuRmjZE3c-vtch62UQiOnpN0). We will be reading [*Let This Radicalize You*](https://url1005.email.actionnetwork.org/ss/c/u001.qUXRBnqZ7T8nxbpAcIVwoUsWLkUMWlo_0JmZttSlm3-gABf-HsnbEYPITLzY9g-r4NA8j6Au8wtK2vUOW7pe6MnkGxw9PBgXLUPoIR8VcEENhJEUtOjsTuKg3pGhiekTJj2bkOuQ4hQ-YtULl0wBz4pa3e3R8ZIHy_fP31pd2JDnsjaHHdMkiCC40RBZveW9jqxocs-mtWpaPHeYvKd1ec9Ok9BCRLqSUDQcp0vElx0xivveDkAVJXQcJREP8bsvGe5TnTS8UJJUXmcTYLCtVNFmnLRIj5xdZPAbCP393cEwBj1Y-4_cPajh9YJI19S0xxbuC1mxRe4ceS2mvMdUYdQf7eReJkYWTLM7IZFcjCoJCbpkJ2ERweWKcoXswAUrX6W4togSCFVGkfXJOtAA1A/4dp/un5WJP1US8uxbaj_G7QlVQ/h9/h001.WBPecB_67DhPskIuj6vueGot_WkyLbLF4jObZNEtAtA) by Kelly Hayes and Mariame Kaba.