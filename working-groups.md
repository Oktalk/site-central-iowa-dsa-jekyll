---
layout: page
title: Working Group
subtitle: Get Involved with a Working Group
---

We have several active working groups, each focusing on key areas of our chapter’s work. Fill out the forms below if you’re interested in joining:

### Working Groups
Communications: [Form](https://actionnetwork.org/forms/central-iowa-dsa-communications-committee-interest-form)

Political Education: [Form](https://actionnetwork.org/forms/central-iowa-dsa-political-education-committee-interest-form)

International: [Form](https://actionnetwork.org/forms/central-iowa-dsa-international-working-group-interest-form)

Housing: [Form](https://actionnetwork.org/forms/housing-working-group-interest-form)

Transit: [Form](https://actionnetwork.org/forms/central-iowa-dsa-transit-wg-interest-form)

Labor: [Form](https://actionnetwork.org/forms/central-iowa-dsa-labor-working-group-interest-form)

### Join DSA

[Join DSA to further the cause of democratic socialism in your town and across the nation.](https://act.dsausa.org/donate/membership2020/).

